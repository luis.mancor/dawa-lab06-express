require("./config/config");   //USO DE DB ATLAS
const express = require("express");

const bodyParser = require("body-parser"); //dependencia instalada POST request //installed sudo npm install --save body-parser for POST ... https://codeforgeek.com/handle-get-post-request-express-4/
const app = express();
const port = process.env.PORT || 3000
const { check, validationResult } = require('express-validator'); //DEPENDENCY FOR VALIDATION **
const methodOverride = require("method-override");  //DEPENDENCY FOR PUT AND DELETE REQUEST VIA FORM  npm install method-override

const mongoose = require("mongoose");
const bcrypt = require("bcrypt");  //DEPENDENCY USED TO ENCRYPT PASSWORDS
const Usuario = require("./models/usuario");    


app.use(express.static(__dirname + '/public'));
app.use(methodOverride('_method'));  //USE OF METHODOVERRIDE

//------------------BODY PARSER DEPENDENCY POST REQUEST-----------//
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
//------------------MONGOSE DEPENDENCY----------------------------// 

mongoose.connect(
    process.env.URLDB,
    { useNewUrlParser: true, useCreateIndex:true, useUnifiedTopology: true },
    (err, res) => {
        if (err) throw err;

        console.log("Base de datos online");
    }  
);

/*
mongoose.connect(
    'mongodb://localhost:27017/lab07DB', (err, res) =>{
    if(err) throw err;
    console.log('Base de datos Online')
});
*/
//----------------------------------------------------------------//

app.set('view engine', 'pug');
app.listen(port, ()=> console.log(`escuchando en puerto ${port}` ));

app.get("/", function(req,res){
    res.render('index')
});
app.get("/contact", function(req,res){
    res.render('contact')
});
app.post('/result', [
    check('nombre').isLength({min: 3}).withMessage('Debe ser un nombre valido de almenos 3 letras.'),
    check('email').isEmail().withMessage('Ingrese un email valido'),
    check('apellido').isLength({min: 3}).withMessage('Campo obligatorio'),
],
function(req, res){
    console.log(req.body)
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        console.log(errors.mapped());
        return res.render('contact', { data: req.body , error: errors.mapped()})
        //return res.status(422).json({errors: errors.mapped()});
        
    }else{
        res.render('result', {
            "data" : {
                "nombre" : req.body.nombre,
                "apellido" : req.body.apellido,
                "telefono" : req.body.telefono,
                "email" : req.body.email,
                "fechaNacimiento" : req.body.fechaNacimiento,
                "descripcion" : req.body.descripcion,
            }
            //data : req.body  WORKING ASWELL
        });
    }
})

// EXPRESS VALIDATOR https://express-validator.github.io/docs/index.html
/*
//METODO DE ENVIO FORMULARIO CON GET  para obtener en la vista es #{data.valor}
app.get("/result", function(req,res){
    //var temp = JSON.parse(JSON.stringify(req.query));
    res.render('result', {
        "data" : {
            "nombre" : req.query.nombre,
            "apellido" : req.query.apellido,
            "telefono" : req.query.telefono,
            "email" : req.query.email,
            "fechaNacimiento" : req.query.fechaNacimiento,
            "descripcion" : req.query.descripcion,
        }
        // "data" : temp
    });
});
*/


app.get("/products", function(req,res){
    res.render('products')
});

app.get("/about", function(req,res){
    res.render('about')
});

//FUNCTION FOR SAVING USER----------------------------------------//
app.post("/usuario", function (req, res){  //FUNCION MOVIDA
    let body = req.body;

    let usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync(body.password, 10),
        role: body.role,
    });
    usuario.save((err, usuarioDB) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.render('usuarioIngresado', {
            "data" : {
                "nombre" : usuarioDB.nombre,
                "email" : usuarioDB.email,
                "password" : usuarioDB.password,
            }
        });
        console.log(usuarioDB.password);
        console.log(usuarioDB);
    });
});

app.get("/userInserted", function(req,res){
    res.render('usuarioIngresado')
});

//FUNCTION FOR LISTING USERS -----------------------------------------------//
app.get("/usuario", function (req, res){
    Usuario.find({}).exec((err, usuarios) => {
        if (err){
            return res.status(400).json({
                ok: false,
                err,
            });
        }

        res.render('listado', {
            "data" : usuarios
        });
        //console.log(JSON.stringify(usuarios));
        //console.log(usuarios[0].nombre);
        console.log(typeof(usuarios));
    });
});
app.get("/lista", function(req,res){
    res.render('listado')
});
//FUNCTION FOR EDITING EXISTING USER---------------------------------------------------//
app.get("/editarUsuario/:id", function(req,res){
    let id = req.params.id;
    let body = req.body;
    console.log(id);  //SHOWS CAPTURED ID FROM URL
    console.log(body);
    
    Usuario.findByIdAndUpdate(id, body, {new: true}, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        res.render('editarUsuario', {
            "data" : usuarioDB
        });
        console.log(usuarioDB);
    });    
});

app.put("/usuario/:id", function (req, res) {
    let id = req.params.id;
    let body = req.body;

    Usuario.findByIdAndUpdate(id, body, {new: true}, (err, usuarioDB) => {
        if (err) {
            return res.status(400).json({
                ok: false,
                err,
            });
        }
        //Render new view with updated data
        Usuario.find({}).exec((err, usuarios) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    err,
                });
            }
    
            res.render('listado', {
                "data" : usuarios
            });
        });
        /*
        res.status(200).json({
            ok: true,
            usuario: usuarioDB,
        });
        */
    });
});

//---FUNCTION FOR DELETING USER BY ID --------------------------------------------------//

app.delete("/usuario/:id", function(req, res){
    let id = req.params.id;
    Usuario.findByIdAndDelete(id, (err, usuarioBorrado)=>{
        if (err){
            return res.status(400).json({
                ok:false,
                err,
            });
        }
        //Render new view with updated data
        Usuario.find({}).exec((err, usuarios) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    err,
                });
            }
    
            res.render('listado', {
                "data" : usuarios
            });
        });
        /*
        res.json({
            ok:true,
            usuario: usuarioBorrado,
        });
        */
    });
});

